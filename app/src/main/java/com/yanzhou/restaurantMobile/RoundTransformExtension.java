package com.yanzhou.restaurantMobile;

import com.bumptech.glide.annotation.GlideExtension;
import com.bumptech.glide.annotation.GlideOption;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by Avengers on 2018/6/29.
 */
@GlideExtension
public class RoundTransformExtension {

    private RoundTransformExtension() {}

    @GlideOption
    public static void roundFit(RequestOptions options, int radius){
        options.transform(new MultiTransformation<>(new FitCenter(), new RoundedCorners(radius)));
    }

    @GlideOption
    public static void roundCrop(RequestOptions options, int radius){
        options.transform(new MultiTransformation<>(new CenterCrop(),new RoundedCorners(radius)));
    }
}
