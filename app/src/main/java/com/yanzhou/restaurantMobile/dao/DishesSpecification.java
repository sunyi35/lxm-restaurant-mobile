package com.yanzhou.restaurantMobile.dao;

/**
 * Created by Avengers on 2018/6/20.
 */
public class DishesSpecification {
    private int dishesSpecificationId;
    private String dishesSpecificationImageUrl;
    private String dishesSpecificationName;

    public int getDishesSpecificationId() {
        return dishesSpecificationId;
    }

    public void setDishesSpecificationId(int dishesSpecificationId) {
        this.dishesSpecificationId = dishesSpecificationId;
    }

    public String getDishesSpecificationImageUrl() {
        return dishesSpecificationImageUrl;
    }

    public void setDishesSpecificationImageUrl(String dishesSpecificationImageUrl) {
        this.dishesSpecificationImageUrl = dishesSpecificationImageUrl;
    }

    public String getDishesSpecificationName() {
        return dishesSpecificationName;
    }

    public void setDishesSpecificationName(String dishesSpecificationName) {
        this.dishesSpecificationName = dishesSpecificationName;
    }

    @Override
    public String toString() {
        return "DishesSpecification{" +
                "dishesSpecificationId=" + dishesSpecificationId +
                ", dishesSpecificationImageUrl='" + dishesSpecificationImageUrl + '\'' +
                ", dishesSpecificationName='" + dishesSpecificationName + '\'' +
                '}';
    }
}
