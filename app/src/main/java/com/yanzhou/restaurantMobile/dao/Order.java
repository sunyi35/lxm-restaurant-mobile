package com.yanzhou.restaurantMobile.dao;

import java.sql.Timestamp;

/**
 * Created by Avengers on 2018/6/20.
 */
public class Order {
    private int orderId;
    private int serialNumber;
    private String orderTips;
    private Timestamp createTime;
    private float totalPrice;
    private boolean finished;

    public float getTotalPrice() {
        return totalPrice;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderTips() {
        return orderTips;
    }

    public void setOrderTips(String orderTips) {
        this.orderTips = orderTips;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", serialNumber=" + serialNumber +
                ", orderTips='" + orderTips + '\'' +
                ", createTime=" + createTime +
                ", totalPrice=" + totalPrice +
                ", finished=" + finished +
                '}';
    }
}
