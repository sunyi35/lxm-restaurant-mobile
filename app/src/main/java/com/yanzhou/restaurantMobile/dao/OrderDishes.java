package com.yanzhou.restaurantMobile.dao;

/**
 * Created by Avengers on 2018/6/20.
 */
public class OrderDishes {
    private int orderDishesId;
    private Order order;
    private Dishes dishes;

    public int getOrderDishesId() {
        return orderDishesId;
    }

    public void setOrderDishesId(int orderDishesId) {
        this.orderDishesId = orderDishesId;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Dishes getDishes() {
        return dishes;
    }

    public void setDishes(Dishes dishes) {
        this.dishes = dishes;
    }
}
