package com.yanzhou.restaurantMobile.dao;

/**
 * Created by Avengers on 2018/6/20.
 */
public class Dishes {
    private int dishesId;
    private String dishesName;
    private String dishesImageUrl;
    private DishesSpecification dishesSpecification;
    private float dishesPrice;
    private boolean disable;

    public float getDishesPrice() {
        return dishesPrice;
    }

    public void setDishesPrice(float dishesPrice) {
        this.dishesPrice = dishesPrice;
    }

    public int getDishesId() {
        return dishesId;
    }

    public void setDishesId(int dishesId) {
        this.dishesId = dishesId;
    }

    public String getDishesName() {
        return dishesName;
    }

    public void setDishesName(String dishesName) {
        this.dishesName = dishesName;
    }

    public String getDishesImageUrl() {
        return dishesImageUrl;
    }

    public void setDishesImageUrl(String dishesImageUrl) {
        this.dishesImageUrl = dishesImageUrl;
    }

    public DishesSpecification getDishesSpecification() {
        return dishesSpecification;
    }

    public void setDishesSpecification(DishesSpecification dishesSpecification) {
        this.dishesSpecification = dishesSpecification;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    @Override
    public String toString() {
        return "Dishes{" +
                "dishesId=" + dishesId +
                ", dishesName='" + dishesName + '\'' +
                ", dishesImageUrl='" + dishesImageUrl + '\'' +
                ", dishesSpecification=" + dishesSpecification +
                ", dishesPrice=" + dishesPrice +
                ", disable=" + disable +
                '}';
    }
}
