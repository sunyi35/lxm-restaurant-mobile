package com.yanzhou.restaurantMobile.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.SparseIntArray;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.yanzhou.restaurantMobile.ClickListenerObserver;
import com.yanzhou.restaurantMobile.R;
import com.yanzhou.restaurantMobile.adapter.DishesAdapter;
import com.yanzhou.restaurantMobile.adapter.OrderAdapter;
import com.yanzhou.restaurantMobile.adapter.SpecificationAdapter;
import com.yanzhou.restaurantMobile.dao.Dishes;
import com.yanzhou.restaurantMobile.dao.DishesSpecification;
import com.yanzhou.restaurantMobile.event.CheckoutEvent;
import com.yanzhou.restaurantMobile.event.DishesEvent;
import com.yanzhou.restaurantMobile.event.SpecificationEvent;
import com.yanzhou.restaurantMobile.retrofit.DishesService;
import com.yanzhou.restaurantMobile.retrofit.DishesSpecificationService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    DishesService dishesService;
    DishesSpecificationService specificationService;
    RecyclerView dishesRecyclerView;
    RecyclerView specificationRecyclerView;
    RecyclerView orderRecyclerView;
    List<Dishes> OrderDishes;
    List<DishesSpecification> specifications;
    List<Dishes> dishesData;
    SparseIntArray dishesSpecificationFirstItemArray;
    FloatingActionButton cartButton;
    DrawerLayout drawerLayout;
    TextView checkoutButton;
    TextView totalPriceView;
    View menuButton;
    View flag;
    View orderLayout;
    View orderLayoutBackground;
    Disposable cartDisposable;
    int statusBarHeight;
    float totalPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_1);
        String url = "http://" + getResources().getString(R.string.ip_address) + "/";
        dishesRecyclerView = findViewById(R.id.dishes_recyclerview);
        specificationRecyclerView = findViewById(R.id.specification_recyclerview);
        orderRecyclerView = findViewById(R.id.order_recyclerview);
        cartButton = findViewById(R.id.cart_button);
        flag = findViewById(R.id.flag);
        drawerLayout = findViewById(R.id.drawer_layout);
        menuButton = findViewById(R.id.menuButton);
        totalPriceView = findViewById(R.id.total_price);
        orderLayout = findViewById(R.id.order_layout);
        orderLayoutBackground = findViewById(R.id.order_layout_background);
        checkoutButton = findViewById(R.id.checkout_button);
        checkoutButton.setEnabled(false);
        checkoutButton.setText(R.string.unselected_text);
        setCartButtonIsEnable(false);
        OrderDishes = new ArrayList<>();
        dishesData = new ArrayList<>();
        dishesSpecificationFirstItemArray = new SparseIntArray();
        totalPrice = 0;
        totalPriceView.setText("¥" + String.valueOf(totalPrice));
        ((SimpleItemAnimator)specificationRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        orderRecyclerView.setAdapter(new OrderAdapter(this));
        orderRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        ((SimpleItemAnimator)orderRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = getResources().getDimensionPixelOffset(resourceId);
        }
            Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        dishesService = retrofit.create(DishesService.class);
        specificationService = retrofit.create(DishesSpecificationService.class);
        specificationService.getAllSpecification().subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).flatMap(new Function<List<DishesSpecification>, ObservableSource<DishesSpecification>>() {
            @Override
            public ObservableSource<DishesSpecification> apply(@NonNull List<DishesSpecification> dishesSpecifications) throws Exception {
                specifications = dishesSpecifications;
                return Observable.fromIterable(dishesSpecifications);
            }
        }).flatMap(new Function<DishesSpecification, ObservableSource<List<Dishes>>>() {
            @Override
            public ObservableSource<List<Dishes>> apply(@NonNull DishesSpecification dishesSpecification) throws Exception {
                return specificationService.getDishesGroupBySpecification(dishesSpecification.getDishesSpecificationId());
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<Dishes>>() {
            @Override
            public void accept(@NonNull List<Dishes> dishes) throws Exception {
                dishesSpecificationFirstItemArray.put(dishes.get(0).getDishesSpecification().getDishesSpecificationId(),dishesData.size());
                dishesData.addAll(dishes);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                throwable.printStackTrace();
            }
        }, new Action() {
            @Override
            public void run() throws Exception {
                specificationRecyclerView.setAdapter(new SpecificationAdapter(specifications, MainActivity.this));
                specificationRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                dishesRecyclerView.setAdapter(new DishesAdapter(dishesData, MainActivity.this));
                dishesRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            }
        });

        orderLayoutBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(v.getVisibility() == View.VISIBLE ? View.INVISIBLE : View.VISIBLE);
                orderLayout.setVisibility(View.GONE);
            }
        });
        Observable.create(new ClickListenerObserver(cartButton)).throttleWithTimeout(200, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(@NonNull Integer integer) throws Exception {
                orderLayout.setVisibility(orderLayout.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                orderLayoutBackground.setVisibility(orderLayoutBackground.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });
        Observable.create(new ClickListenerObserver(checkoutButton)).throttleWithTimeout(400, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(@NonNull Integer i) throws Exception {
                Intent intent = new Intent(MainActivity.this,CheckOutActivity.class);
                startActivity(intent);
                EventBus.getDefault().postSticky(new CheckoutEvent.CreateEvent(getOrdersDishes()));
            }
        });
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerLayout.isDrawerOpen(Gravity.START)){
                    drawerLayout.openDrawer(Gravity.START,true);
                }
            }
        });
        dishesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState != RecyclerView.SCROLL_STATE_IDLE){
                    if (cartDisposable != null) cartDisposable.dispose();
                    if (cartButton != null && cartButton.isShown()){
                        cartButton.hide();
                    }
                }else{
                    if (cartButton != null) {
                        Observable.timer(300,TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Long>() {
                            @Override
                            public void onSubscribe(@NonNull Disposable d) {
                                cartDisposable = d;
                            }

                            @Override
                            public void onNext(@NonNull Long aLong) {
                                cartButton.show();
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSpecificationClickEvent(SpecificationEvent.ClickEvent event){
        drawerLayout.closeDrawer(Gravity.START,true);
        if (dishesRecyclerView.getLayoutManager() instanceof LinearLayoutManager){
            ((LinearLayoutManager)dishesRecyclerView.getLayoutManager()).scrollToPositionWithOffset(dishesSpecificationFirstItemArray.get(event.getSpecificationId()),0);
        }else {
            dishesRecyclerView.scrollToPosition(dishesSpecificationFirstItemArray.get(event.getSpecificationId()));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDishesClickEvent(DishesEvent.ClickEvent event){
        OrderDishes.add(event.getDishes());
        totalPrice += event.getDishes().getDishesPrice();
        if (OrderDishes != null && OrderDishes.size() > 0){
            checkoutButton.setEnabled(true);
            checkoutButton.setText(R.string.selected_text);
            setCartButtonIsEnable(true);
        }
        totalPriceView.setText("¥" + String.valueOf(totalPrice));
        addDishesAnimation(event.getLocation());
        ((OrderAdapter)orderRecyclerView.getAdapter()).setDishes(getOrdersDishes());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onIncreaseEvent(DishesEvent.IncreaseEvent event){
        for (Dishes dishes : OrderDishes){
            if (dishes.getDishesId() == event.getDishesId()){
                Dishes newDishes = new Dishes();
                newDishes.setDishesId(dishes.getDishesId());
                newDishes.setDisable(dishes.isDisable());
                newDishes.setDishesImageUrl(dishes.getDishesImageUrl());
                newDishes.setDishesName(dishes.getDishesName());
                newDishes.setDishesSpecification(dishes.getDishesSpecification());
                newDishes.setDishesPrice(dishes.getDishesPrice());
                OrderDishes.add(newDishes);
                totalPrice += newDishes.getDishesPrice();
                totalPriceView.setText("¥" + String.valueOf(totalPrice));
                break;
            }
        }
        if (OrderDishes != null && OrderDishes.size() > 0){
            checkoutButton.setEnabled(true);
            checkoutButton.setText(R.string.selected_text);
            setCartButtonIsEnable(true);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReduceEvent(DishesEvent.ReduceEvent event){
        for (Dishes dishes : OrderDishes){
            if (dishes.getDishesId() == event.getDishesId()){
                OrderDishes.remove(dishes);
                totalPrice -= dishes.getDishesPrice();
                totalPriceView.setText("¥" + String.valueOf(totalPrice));
                break;
            }
        }
        if (OrderDishes != null && OrderDishes.size() > 0){
        }else {
            orderLayout.setVisibility(View.GONE);
            orderLayoutBackground.setVisibility(View.GONE);
            checkoutButton.setEnabled(false);
            checkoutButton.setText(R.string.unselected_text);
            setCartButtonIsEnable(false);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadMoreEvent(DishesEvent.LoadMoreEvent event){

    }

    private List<List<Dishes>> getOrdersDishes(){
        List<List<Dishes>> dishesList = new ArrayList<>();
        boolean noSame;
        for (Dishes dishes : OrderDishes){
            noSame = true;
            for (List<Dishes> dishesSame : dishesList){
                if (dishes.getDishesId() == dishesSame.get(0).getDishesId()){
                    dishesSame.add(dishes);
                    noSame = false;
                    break;
                }
            }
            if (noSame){
                List<Dishes> newDishesList = new ArrayList<>();
                newDishesList.add(dishes);
                dishesList.add(newDishesList);
            }
        }
        return dishesList;
    }

    private void setCartButtonIsEnable(boolean enable){
        if (cartButton != null){
            if (enable){
                cartButton.setEnabled(true);
                cartButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#f2673b")));
            }else {
                cartButton.setEnabled(false);
                cartButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#D2D2D2")));
            }
        }
    }


    private void addDishesAnimation(int[] eventLocation){
        int location[] = new int[2];
        int location2[] = new int[2];
        flag.getLocationInWindow(location);
        cartButton.getLocationInWindow(location2);
        int x1 = location[0];
        int y1 = location[1];
        int x2 = location2[0];
        int y2 = location2[1];
        ObjectAnimator translateAnimationX = ObjectAnimator.ofFloat(flag, "translationX", eventLocation[0], -(x1 - x2));
        translateAnimationX.setDuration(200);
        translateAnimationX.setInterpolator(new LinearInterpolator());
        ObjectAnimator translateAnimationY = ObjectAnimator.ofFloat(flag, "translationY", eventLocation[1] - statusBarHeight, y2 - y1);
        translateAnimationY.setDuration(200);
        translateAnimationY.setInterpolator(new AccelerateInterpolator());
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(translateAnimationX).with(translateAnimationY);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                flag.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                flag.clearAnimation();
                flag.setTranslationX(0);
                flag.setTranslationY(0);
                flag.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animatorSet.start();
    }
}
