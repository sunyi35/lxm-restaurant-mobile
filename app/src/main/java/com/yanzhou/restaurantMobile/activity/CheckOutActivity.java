package com.yanzhou.restaurantMobile.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yanzhou.restaurantMobile.ClickListenerObserver;
import com.yanzhou.restaurantMobile.R;
import com.yanzhou.restaurantMobile.adapter.CheckoutAdapter;
import com.yanzhou.restaurantMobile.dao.Dishes;
import com.yanzhou.restaurantMobile.dao.Order;
import com.yanzhou.restaurantMobile.dao.OrderDishes;
import com.yanzhou.restaurantMobile.event.CheckoutEvent;
import com.yanzhou.restaurantMobile.event.ResultEvent;
import com.yanzhou.restaurantMobile.retrofit.DishesService;
import com.yanzhou.restaurantMobile.retrofit.OrderService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class CheckOutActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    View confirmButton;
    TextView totalPriceView;
    View backButton;
    List<List<Dishes>> dishes;
    View loadingLayout;
    View orderTypeLayout;
    View outsideButton;
    View insideButton;
    OrderService orderService;
    DishesService dishesService;
    int orderId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out_1);
        String url = "http://" + getResources().getString(R.string.ip_address) + "/";
        recyclerView = findViewById(R.id.recyclerview);
        confirmButton = findViewById(R.id.confirm_button);
        totalPriceView = findViewById(R.id.total_price);
        backButton = findViewById(R.id.back_button);
        loadingLayout = findViewById(R.id.loading_layout);
        orderTypeLayout = findViewById(R.id.order_type_layout);
        outsideButton = findViewById(R.id.outside_button);
        insideButton = findViewById(R.id.inside_button);
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        orderService = retrofit.create(OrderService.class);
        dishesService = retrofit.create(DishesService.class);
        dishes = new ArrayList<>();
        Observable.create(new ClickListenerObserver(confirmButton)).throttleWithTimeout(400, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(@NonNull Integer integer) throws Exception {
                loadingLayout.setVisibility(View.VISIBLE);
                confirmButton.setEnabled(false);
                Order order = new Order();
                order.setTotalPrice(0.0f);
                if (((CheckoutAdapter)recyclerView.getAdapter()).getOrderTips() != null && ((CheckoutAdapter)recyclerView.getAdapter()).getOrderTips().length() > 0){
                    order.setOrderTips(((CheckoutAdapter)recyclerView.getAdapter()).isSendFood() ? "送餐" : "堂吃" + ";" + ((CheckoutAdapter)recyclerView.getAdapter()).getOrderTips());
                }else {
                    order.setOrderTips(((CheckoutAdapter)recyclerView.getAdapter()).isSendFood() ? "送餐" : "堂吃");
                }
                List<OrderDishes> orderDishesList = new ArrayList<>();
                for (List<Dishes> dishesList : dishes){
                    for (Dishes dishes : dishesList){
                        OrderDishes orderDishes = new OrderDishes();
                        orderDishes.setDishes(dishes);
                        orderDishes.setOrder(order);
                        orderDishesList.add(orderDishes);
                    }
                }
                orderService.createOrder(orderDishesList).subscribeOn(Schedulers.io()).filter(new Predicate<String>() {
                    @Override
                    public boolean test(@NonNull String s) throws Exception {
                        if (s.equals("创建失败") && Integer.valueOf(s) <= 0){
                            throw new RuntimeException(s);
                        }else {
                            return true;
                        }
                    }
                }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<String>() {
                    @Override
                    public void accept(@NonNull String s) throws Exception {
                            loadingLayout.setVisibility(View.GONE);
                            Intent intent = new Intent(CheckOutActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            Intent intent1 = new Intent(CheckOutActivity.this, ResultActivity.class);
                            startActivity(intent1);
                            EventBus.getDefault().postSticky(new ResultEvent.CreateEvent(Integer.valueOf(s)));
                            confirmButton.setEnabled(true);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        loadingLayout.setVisibility(View.GONE);
                        Intent intent = new Intent(CheckOutActivity.this,FailureActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onCreateEvent(final CheckoutEvent.CreateEvent event){
        final SparseBooleanArray ableDishes = new SparseBooleanArray();
        Observable.fromIterable(event.getDishes()).observeOn(Schedulers.io()).flatMap(new Function<List<Dishes>, ObservableSource<Dishes>>() {
            @Override
            public ObservableSource<Dishes> apply(@NonNull List<Dishes> dishes) throws Exception {
                return dishesService.getDishes(dishes.get(0).getDishesId());
            }
        }).filter(new Predicate<Dishes>() {
            @Override
            public boolean test(@NonNull Dishes dishes) throws Exception {
                return dishes.isDisable();
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Dishes>() {
            @Override
            public void accept(@NonNull Dishes dishes) throws Exception {
                ableDishes.put(dishes.getDishesId(),true);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                throwable.printStackTrace();
            }
        }, new Action() {
            @Override
            public void run() throws Exception {
                for (List<Dishes> dishesList : event.getDishes()){
                    if (ableDishes.get(dishesList.get(0).getDishesId())) dishes.add(dishesList);
                }
                recyclerView.setAdapter(new CheckoutAdapter(dishes,CheckOutActivity.this));
                recyclerView.setLayoutManager(new LinearLayoutManager(CheckOutActivity.this));
                ((SimpleItemAnimator)recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
                totalPriceView.setText("¥" + String.valueOf(getTotalPrice()));
            }
        });
        EventBus.getDefault().removeStickyEvent(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChooseOrderType(CheckoutEvent.ChooseOrderType event){
        orderTypeLayout.setVisibility(View.VISIBLE);
    }

    private float getTotalPrice(){
        float totalPrice = 0f;
        for (List<Dishes> dishesList : dishes){
            totalPrice += dishesList.get(0).getDishesPrice() * dishesList.size();
        }
        return totalPrice;
    }
}
