package com.yanzhou.restaurantMobile.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yanzhou.restaurantMobile.R;
import com.yanzhou.restaurantMobile.dao.Order;
import com.yanzhou.restaurantMobile.event.ResultEvent;
import com.yanzhou.restaurantMobile.retrofit.OrderService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResultActivity extends AppCompatActivity {

    int serialNumber;
    ViewGroup serialNumberLayout;
    View homeButton;
    OrderService orderService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        String url = "http://" + getResources().getString(R.string.ip_address) + "/";
        serialNumberLayout = findViewById(R.id.serial_number_layout);
        homeButton = findViewById(R.id.home_button);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        orderService = retrofit.create(OrderService.class);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void onCreateEvent(ResultEvent.CreateEvent event){
        orderService.getOrder(event.getOrderId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Order>() {
                    @Override
                    public void accept(@NonNull Order order) throws Exception {
                        serialNumber = order.getSerialNumber();
                        for (int i = 1; i <= serialNumberLayout.getChildCount() && serialNumber > 0; i++) {
                            ((TextView) serialNumberLayout.getChildAt(serialNumberLayout.getChildCount() - i)).setText(String.valueOf(serialNumber % 10));
                            serialNumber /= 10;
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                    }
                });
        EventBus.getDefault().removeStickyEvent(event);
    }
}
