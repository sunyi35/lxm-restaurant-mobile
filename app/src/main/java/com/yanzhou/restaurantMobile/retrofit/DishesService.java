package com.yanzhou.restaurantMobile.retrofit;

import com.yanzhou.restaurantMobile.dao.Dishes;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Avengers on 2018/6/25.
 */

public interface DishesService {

    @GET("dishes/{dishesId}")
    Observable<Dishes> getDishes(@Path("dishesId") int dishesId);

    @GET("dishes")
    Observable<List<Dishes>> getAllDishes();

    @GET("dishes")
    Observable<List<Dishes>> getDishesByStatus(@Query("disable") boolean disable);

    @PUT("dishes/{dishesId}")
    Observable<String> editDishes(@Path("dishesId") int dishesId);

    @DELETE("dishes/{dishesId}")
    Observable<String> removeDishes(@Path("dishesId") int dishesId);
}
