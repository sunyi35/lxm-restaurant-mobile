package com.yanzhou.restaurantMobile.retrofit;

import com.yanzhou.restaurantMobile.dao.Dishes;
import com.yanzhou.restaurantMobile.dao.Order;
import com.yanzhou.restaurantMobile.dao.OrderDishes;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Avengers on 2018/6/23.
 */

public interface OrderService {

    @GET("orders/{orderId}")
    Observable<Order> getOrder(@Path("orderId") int orderId);

    @GET("orders")
    Observable<List<Order>> getAllOrders();

    @GET("orders")
    Observable<List<Order>> getOrdersByStatus(@Query("finished") boolean finished);

    @GET("orders/{orderId}/dishes")
    Observable<List<Dishes>> getOrderAllDishes(@Path("orderId") int orderId);

    @POST("orders")
    Observable<String> createOrder(@Body List<OrderDishes> orderDishes);

    @POST("orders/{orderId}/dishes")
    Observable<String> addOrderDishes(@Path("orderId") int orderId, @Body List<String> json);

    @PUT("orders/{orderId}")
    Observable<String> editOrder(@Path("orderId") int orderId);

    @DELETE("orders/{orderId}")
    Observable<String> removeOrder(@Path("orderId") int orderId);

    @DELETE("orders/{orderId}/dishes")
    Observable<String> removeOrderAllDishes(@Path("orderId") int orderId);

    @DELETE("orders/{orderId}/dishes/{dishesId}")
    Observable<String> removeOrderDishes(@Path("orderId") int orderId, @Path("dishesId") int dishesId);
}
