package com.yanzhou.restaurantMobile.retrofit;

import com.yanzhou.restaurantMobile.dao.Dishes;
import com.yanzhou.restaurantMobile.dao.DishesSpecification;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Avengers on 2018/6/25.
 */

public interface DishesSpecificationService {

    @GET("specifications")
    Observable<List<DishesSpecification>> getAllSpecification();

    @GET("specifications/{specificationId}")
    Observable<DishesSpecification> getSpecification(@Path("specificationId") int specificationId);

    @GET("specifications/{specificationId}/dishes")
    Observable<List<Dishes>> getDishesGroupBySpecification(@Path("specificationId") int specificationId);

    @PUT("specifications/{specificationId}")
    Observable<String> editSpecification(@Path("specificationId") int specificationId);

    @DELETE("specifications/{specificationId}")
    Observable<String> removeSpecification(@Path("specificationId") int specificationId);
}
