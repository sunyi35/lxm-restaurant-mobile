package com.yanzhou.restaurantMobile;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Avengers on 2017/7/7.
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {
}
