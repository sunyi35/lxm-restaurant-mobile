package com.yanzhou.restaurantMobile.event;

/**
 * Created by Avengers on 2018/6/28.
 */

public class SpecificationEvent {
    public static class ClickEvent{
        private int specificationId;

        public int getSpecificationId() {
            return specificationId;
        }

        public ClickEvent(int specificationId) {

            this.specificationId = specificationId;
        }
    }
}
