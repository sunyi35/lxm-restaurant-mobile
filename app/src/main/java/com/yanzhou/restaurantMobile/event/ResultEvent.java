package com.yanzhou.restaurantMobile.event;

/**
 * Created by Avengers on 2018/7/3.
 */

public class ResultEvent {
    public static class CreateEvent{
        int orderId;

        public int getOrderId() {
            return orderId;
        }

        public CreateEvent(int orderId) {

            this.orderId = orderId;
        }
    }
}
