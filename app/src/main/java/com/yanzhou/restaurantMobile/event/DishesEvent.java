package com.yanzhou.restaurantMobile.event;

import com.yanzhou.restaurantMobile.dao.Dishes;

/**
 * Created by Avengers on 2018/6/28.
 */

public class DishesEvent {
    public static class ClickEvent{
        private Dishes dishes;
        private int location[];

        public ClickEvent(Dishes dishes, int[] location) {
            this.dishes = dishes;
            this.location = location;
        }

        public Dishes getDishes() {
            return dishes;
        }

        public int[] getLocation() {
            return location;
        }
    }

    public static class IncreaseEvent{
        private int dishesId;

        public IncreaseEvent(int dishesId) {
            this.dishesId = dishesId;
        }

        public int getDishesId() {
            return dishesId;
        }
    }
    public static class ReduceEvent{
        private int dishesId;

        public ReduceEvent(int dishesId) {
            this.dishesId = dishesId;
        }

        public int getDishesId() {
            return dishesId;
        }
    }

    public static class LoadMoreEvent{

    }
}
