package com.yanzhou.restaurantMobile.event;

import com.yanzhou.restaurantMobile.dao.Dishes;

import java.util.List;

/**
 * Created by Avengers on 2018/6/28.
 */

public class CheckoutEvent {
    public static class CreateEvent{
        private List<List<Dishes>> dishes;

        public CreateEvent(List<List<Dishes>> dishes) {
            this.dishes = dishes;
        }

        public List<List<Dishes>> getDishes() {
            return dishes;
        }
    }

    public static class ChooseOrderType{}
}
