package com.yanzhou.restaurantMobile;

import android.view.View;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;

/**
 * Created by Avengers on 2018/9/28.
 */

public class ClickListenerObserver implements ObservableOnSubscribe<Integer>{

    private View view;

    public ClickListenerObserver(View view) {
        this.view = view;
    }

    @Override
    public void subscribe(@NonNull final ObservableEmitter e) throws Exception {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                e.onNext(0);
            }
        });
    }
}
