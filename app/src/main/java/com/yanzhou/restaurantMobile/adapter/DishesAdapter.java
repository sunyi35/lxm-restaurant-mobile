package com.yanzhou.restaurantMobile.adapter;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yanzhou.restaurantMobile.ClickListenerObserver;
import com.yanzhou.restaurantMobile.GlideApp;
import com.yanzhou.restaurantMobile.R;
import com.yanzhou.restaurantMobile.dao.Dishes;
import com.yanzhou.restaurantMobile.event.DishesEvent;

import org.greenrobot.eventbus.EventBus;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Avengers on 2018/6/26.
 */

public class DishesAdapter extends RecyclerView.Adapter {

    private List<Dishes> dishes;
    private Context mContext;


    public DishesAdapter(List<Dishes> dishes, Context mContext) {
        this.dishes = dishes;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final ItemHolderView holderView = new ItemHolderView(LayoutInflater.from(mContext).inflate(R.layout.dishes_recyclerview_item_1,parent,false));
        Observable.create(new ClickListenerObserver(holderView.itemView)).throttleWithTimeout(200, TimeUnit.MILLISECONDS, Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(@NonNull Integer i) throws Exception {
                int location[] = new int[2];
                holderView.dishesIncreaseButton.getLocationInWindow(location);
                EventBus.getDefault().post(new DishesEvent.ClickEvent(dishes.get(holderView.getLayoutPosition()),location));
            }
        });
        return holderView;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ((ItemHolderView)holder).dishesName.setText(dishes.get(position).getDishesName());
        ((ItemHolderView) holder).dishesPrice.setText("¥" + dishes.get(position).getDishesPrice());
        GlideApp.with(mContext).load(dishes.get(position).getDishesImageUrl()).centerCrop().into(((ItemHolderView) holder).dishesImage);
    }

    @Override
    public int getItemCount() {
        return dishes.size();
    }

    public void setDishes(List<Dishes> dishes){
        this.dishes = dishes;
        notifyDataSetChanged();
    }

    private class ItemHolderView extends ViewHolder {

        ImageView dishesImage;
        TextView dishesName;
        TextView dishesPrice;
        View dishesIncreaseButton;

        public ItemHolderView(View itemView) {
            super(itemView);
            dishesImage = itemView.findViewById(R.id.dishes_image);
            dishesName = itemView.findViewById(R.id.dishes_name);
            dishesPrice = itemView.findViewById(R.id.dishes_price);
            dishesIncreaseButton = itemView.findViewById(R.id.dishes_increase_button);
        }
    }
}
