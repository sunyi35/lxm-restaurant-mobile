package com.yanzhou.restaurantMobile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yanzhou.restaurantMobile.R;
import com.yanzhou.restaurantMobile.dao.Dishes;
import com.yanzhou.restaurantMobile.event.CheckoutEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Avengers on 2018/6/28.
 */

public class CheckoutAdapter extends RecyclerView.Adapter {

    List<List<Dishes>> dishes;
    Context mContext;
    private boolean isSendFood;
    private String orderTips;
    private final static int FOOTER = 2;
    private final static int ITEM = 1;

    public CheckoutAdapter(List<List<Dishes>> dishes, Context mContext) {
        this.dishes = dishes;
        this.mContext = mContext;
        isSendFood = false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == FOOTER){
            FooterViewHolder footerViewHolder = new FooterViewHolder(LayoutInflater.from(mContext).inflate(R.layout.checkout_recyclerview_footer,parent,false));
            footerViewHolder.chooseTypeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new CheckoutEvent.ChooseOrderType());
                }
            });
            footerViewHolder.orderTipsEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    orderTips = s.toString();
                }
            });
            footerViewHolder.sendFoodButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isSendFood = ((CheckBox)v).isChecked();
                }
            });
            return footerViewHolder;
        }else if (viewType == ITEM){
            ItemViewHolder itemViewHolder = new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.checkout_recyclerview_item,parent,false));
            return itemViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FooterViewHolder){
            ((FooterViewHolder) holder).sendFoodButton.setChecked(isSendFood);
        }else if(holder instanceof ItemViewHolder){
            float price = dishes.get(position).get(0).getDishesPrice() * dishes.get(position).size();
            ((ItemViewHolder) holder).dishesPrice.setText(String.valueOf(price));
            ((ItemViewHolder) holder).dishesNumber.setText("x" + dishes.get(position).size());
            ((ItemViewHolder) holder).dishesName.setText(dishes.get(position).get(0).getDishesName());
            Glide.with(mContext).load(dishes.get(position).get(0).getDishesImageUrl()).into(((ItemViewHolder) holder).dishesImage);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position < dishes.size() ? ITEM : FOOTER;
    }

    @Override
    public int getItemCount() {
        return dishes.size() + 1;
    }

    public boolean isSendFood(){
        return isSendFood;
    }

    public void setIsSendFood(boolean isSendFood){
        this.isSendFood = isSendFood;
        notifyItemChanged(getItemCount() - 1);
    }

    public String getOrderTips(){
        return orderTips;
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView dishesName;
        TextView dishesNumber;
        TextView dishesPrice;
        ImageView dishesImage;

        public ItemViewHolder(View itemView) {
            super(itemView);
            dishesName = itemView.findViewById(R.id.dishes_name);
            dishesNumber = itemView.findViewById(R.id.dishes_number);
            dishesPrice = itemView.findViewById(R.id.dishes_price);
            dishesImage = itemView.findViewById(R.id.dishes_image);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {

        CheckBox sendFoodButton;
        View chooseTypeButton;
        EditText orderTipsEdit;

        public FooterViewHolder(View itemView) {
            super(itemView);
            sendFoodButton = itemView.findViewById(R.id.sendFoodButton);
            chooseTypeButton = itemView.findViewById(R.id.choose_type_button);
            orderTipsEdit = itemView.findViewById(R.id.order_tips_edit);
        }
    }
}
