package com.yanzhou.restaurantMobile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yanzhou.restaurantMobile.ClickListenerObserver;
import com.yanzhou.restaurantMobile.GlideApp;
import com.yanzhou.restaurantMobile.R;
import com.yanzhou.restaurantMobile.dao.Dishes;
import com.yanzhou.restaurantMobile.event.DishesEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

/**
 * Created by Avengers on 2018/6/29.
 */

public class OrderAdapter extends RecyclerView.Adapter {

    List<List<Dishes>> dishes;
    Context mContext;

    public OrderAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final ItemViewHolder holder = new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.order_recyclerview_item,parent,false));
        Observable.create(new ClickListenerObserver(holder.increaseButton)).throttleWithTimeout(200, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(@NonNull Integer integer) throws Exception {
                Dishes newDishes = new Dishes();
                newDishes.setDishesId(dishes.get(holder.getLayoutPosition()).get(0).getDishesId());
                newDishes.setDisable(dishes.get(holder.getLayoutPosition()).get(0).isDisable());
                newDishes.setDishesImageUrl(dishes.get(holder.getLayoutPosition()).get(0).getDishesImageUrl());
                newDishes.setDishesName(dishes.get(holder.getLayoutPosition()).get(0).getDishesName());
                newDishes.setDishesSpecification(dishes.get(holder.getLayoutPosition()).get(0).getDishesSpecification());
                newDishes.setDishesPrice(dishes.get(holder.getLayoutPosition()).get(0).getDishesPrice());
                dishes.get(holder.getLayoutPosition()).add(newDishes);
                EventBus.getDefault().post(new DishesEvent.IncreaseEvent(newDishes.getDishesId()));
                notifyItemChanged(holder.getLayoutPosition());
            }
        });
        Observable.create(new ClickListenerObserver(holder.reduceButton)).throttleWithTimeout(200, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(@NonNull Integer integer) throws Exception {
                if (dishes.get(holder.getLayoutPosition()).size() > 1){
                    dishes.get(holder.getLayoutPosition()).remove(0);
                    EventBus.getDefault().post(new DishesEvent.ReduceEvent(dishes.get(holder.getLayoutPosition()).get(0).getDishesId()));
                    notifyItemChanged(holder.getLayoutPosition(),1);
                }else {
                    EventBus.getDefault().post(new DishesEvent.ReduceEvent(dishes.get(holder.getLayoutPosition()).get(0).getDishesId()));
                    dishes.remove(holder.getLayoutPosition());
                    notifyItemChanged(holder.getLayoutPosition());
                    notifyItemRemoved(holder.getLayoutPosition());
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List payloads) {
        if (payloads.isEmpty()){
            ((ItemViewHolder)holder).dishesName.setText(dishes.get(position).get(0).getDishesName());
            ((ItemViewHolder) holder).dishesPrice.setText("¥" + dishes.get(position).get(0).getDishesPrice() * dishes.get(position).size());
            ((ItemViewHolder) holder).dishesNumber.setText(String.valueOf(dishes.get(position).size()));
            GlideApp.with(mContext).load(dishes.get(position).get(0).getDishesImageUrl()).roundCrop(6).into(((ItemViewHolder) holder).dishesImage);
        }else {
            ((ItemViewHolder) holder).dishesNumber.setText(String.valueOf(dishes.get(position).size()));
            ((ItemViewHolder) holder).dishesPrice.setText("¥" + dishes.get(position).get(0).getDishesPrice() * dishes.get(position).size());
        }
    }

    @Override
    public int getItemCount() {
        return dishes != null ? dishes.size() : 0;
    }

    public void setDishes(List<List<Dishes>> dishes){
        this.dishes = dishes;
        notifyDataSetChanged();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        ImageView dishesImage;
        ImageView reduceButton;
        ImageView increaseButton;
        TextView dishesName;
        TextView dishesPrice;
        TextView dishesNumber;

        public ItemViewHolder(View itemView) {
            super(itemView);
            dishesImage = itemView.findViewById(R.id.dishes_image);
            reduceButton = itemView.findViewById(R.id.reduce_button);
            increaseButton = itemView.findViewById(R.id.increase_button);
            dishesName = itemView.findViewById(R.id.dishes_name);
            dishesPrice = itemView.findViewById(R.id.dishes_price);
            dishesNumber = itemView.findViewById(R.id.dishes_number);
        }
    }
}
