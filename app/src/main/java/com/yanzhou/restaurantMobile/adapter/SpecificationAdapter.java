package com.yanzhou.restaurantMobile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yanzhou.restaurantMobile.ClickListenerObserver;
import com.yanzhou.restaurantMobile.GlideApp;
import com.yanzhou.restaurantMobile.R;
import com.yanzhou.restaurantMobile.dao.DishesSpecification;
import com.yanzhou.restaurantMobile.event.SpecificationEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

/**
 * Created by Avengers on 2018/6/28.
 */

public class SpecificationAdapter extends RecyclerView.Adapter {

    List<DishesSpecification> specifications;
    Context mContext;

    public SpecificationAdapter(List<DishesSpecification> specifications, Context mContext) {
        this.specifications = specifications;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final ItemViewHolder holder = new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.specification_recyclerview_item,parent,false));
        Observable.create(new ClickListenerObserver(holder.itemView)).throttleWithTimeout(200, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(@NonNull Integer integer) throws Exception {
                EventBus.getDefault().post(new SpecificationEvent.ClickEvent(specifications.get(holder.getLayoutPosition()).getDishesSpecificationId()));
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).specificationName.setText(specifications.get(position).getDishesSpecificationName());
        GlideApp.with(mContext).load(specifications.get(position).getDishesSpecificationImageUrl()).circleCrop().into(((ItemViewHolder) holder).specificationImage);
    }

    @Override
    public int getItemCount() {
        return specifications.size();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView specificationName;
        ImageView specificationImage;

        public ItemViewHolder(View itemView) {
            super(itemView);
            specificationName = itemView.findViewById(R.id.specification_name);
            specificationImage = itemView.findViewById(R.id.specification_image);
        }
    }
}
